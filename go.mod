module chainmaker.org/chainmaker/utils/v2

go 1.15

require (
	chainmaker.org/chainmaker/common/v2 v2.2.1
	chainmaker.org/chainmaker/pb-go/v2 v2.2.1
	chainmaker.org/chainmaker/protocol/v2 v2.2.2
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.6.0
	github.com/google/uuid v1.1.2
	github.com/mr-tron/base58 v1.2.0
	github.com/pingcap/parser v0.0.0-20200623164729-3a18f1e5dceb
	github.com/stretchr/testify v1.7.0
	github.com/studyzy/sqlparse v0.0.0-20210520090832-d40c792e1576
)
